package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dto.Calcular;
import exceptions.Excepciones;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JLabel;

public class Principal extends JFrame {

	private JPanel contentPane;
	
	JButton ce;
		
	JButton nueve;
	
	JButton seis;
	
	JButton coma;
	
	JButton ocho;
	
	JButton cinco;
	
	JButton cero;
	
	JButton siete;
	
	JButton cuatro;
	
	JButton uno;
	
	JButton dos;
	
	JButton tres;
	 
	String valor = "";
	
	double numero = 0;		
	
	double resultado = 0;
	
	String resultados = "";
			
	JTextField numeroIntroducido;
	
	JComboBox monedaEntrante;
	
	JTextField mostrarResultado;
	
	JComboBox monedaResultado;
	
	String monedaE = "";
	
	String monedaR = "";
	
	Calcular c = new Calcular();
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 628, 545);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		
		ce = new JButton("CE");
		ce.setBounds(178, 168, 71, 31);
		ce.addActionListener(pulsarCE);		
		
		JButton delete = new JButton("Delete");				
		delete.setBounds(459, 139, 85, 31);
		contentPane.add(delete);
		delete.addActionListener(pulsarCE);
		
		 nueve = new JButton("9");
		nueve.setBounds(458, 181, 71, 31);
		contentPane.add(nueve);
		nueve.addActionListener(pulsarBoton);
		
		 seis = new JButton("6");
		seis.setBounds(458, 223, 71, 31);
		contentPane.add(seis);
		seis.addActionListener(pulsarBoton);
		
		 coma = new JButton(",");
		coma.setBounds(458, 315, 71, 31);
		contentPane.add(coma);
		coma.addActionListener(pulsarComa);
		
		 ocho = new JButton("8");
		ocho.setBounds(377, 181, 71, 31);
		contentPane.add(ocho);
		ocho.addActionListener(pulsarBoton);
		
		 cinco = new JButton("5");
		cinco.setBounds(377, 227, 71, 31);
		contentPane.add(cinco);
		cinco.addActionListener(pulsarBoton);
		
		 cero = new JButton("0");
		cero.setBounds(377, 315, 71, 31);
		contentPane.add(cero);
		cero.addActionListener(pulsarBoton);
		
		 siete = new JButton("7");
		siete.setBounds(296, 181, 71, 31);
		contentPane.add(siete);
		siete.addActionListener(pulsarBoton);
		
		 cuatro = new JButton("4");
		cuatro.setBounds(296, 227, 71, 31);
		contentPane.add(cuatro);
		cuatro.addActionListener(pulsarBoton);

		uno = new JButton("1");
		uno.setBounds(296, 269, 71, 31);
		contentPane.add(uno);
		uno.addActionListener(pulsarBoton);

		dos = new JButton("2");
		dos.setBounds(377, 269, 71, 31);
		contentPane.add(dos);
		dos.addActionListener(pulsarBoton);

		tres = new JButton("3");
		tres.setBounds(458, 269, 71, 31);
		contentPane.add(tres);
		tres.addActionListener(pulsarBoton);
		
		numeroIntroducido = new JTextField();
		numeroIntroducido.setBounds(52, 120, 86, 38);
		contentPane.add(numeroIntroducido);
		numeroIntroducido.setColumns(10);
		
		monedaEntrante = new JComboBox();
		monedaEntrante.setToolTipText("");
		monedaEntrante.setBounds(52, 174, 154, 20);
		contentPane.add(monedaEntrante);
		monedaEntrante.addActionListener(cambiarMoneda);

		
		
		mostrarResultado = new JTextField();
		mostrarResultado.setColumns(10);
		mostrarResultado.setBounds(52, 262, 86, 38);
		contentPane.add(mostrarResultado);

		
		monedaResultado = new JComboBox();
		monedaResultado.setToolTipText("");
		monedaResultado.setBounds(52, 326, 154, 20);
		contentPane.add(monedaResultado);
		monedaResultado.addActionListener(cambiarMoneda);

		
		c.llenarMonedas(monedaEntrante);
		c.llenarMonedas(monedaResultado);

		
		
	}
	
	
	//EVENTOS	
			ActionListener pulsarBoton = new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						
						JButton aux = (JButton) e.getSource();							
						numeroIntroducido.setText(numeroIntroducido.getText()+aux.getText());//Mostramos valores pulsados
						numero = Double.parseDouble(numeroIntroducido.getText());
						monedaE = monedaEntrante.getSelectedItem().toString();
						monedaR = monedaResultado.getSelectedItem().toString();
						resultado = c.calculoCambio(monedaE, monedaR, numero);
						
						mostrarResultado.setText(String.format("%.2f", resultado));

						
					}
				};
				
				ActionListener cambiarMoneda = new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						if(numero!=0) {
							numero = Double.parseDouble(numeroIntroducido.getText());
							monedaE = monedaEntrante.getSelectedItem().toString();
							monedaR = monedaResultado.getSelectedItem().toString();
							resultado = c.calculoCambio(monedaE, monedaR, numero);
							mostrarResultado.setText(String.format("%.2f", resultado));
						}


						
					}
				};
				
				
				
						

				
				ActionListener pulsarCE = new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						valor = numeroIntroducido.getText();
						String cadenaNueva = "";
							for (int i = 0; i < valor.length()-1; i++) {
								cadenaNueva = cadenaNueva + valor.charAt(i);//Borramos el ultimo caracter
							
						}
							numeroIntroducido.setText(cadenaNueva);
																	
					}
				};
				
								
				
				ActionListener pulsarComa= new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {	
						numeroIntroducido.setText(numeroIntroducido.getText()+".");//Convertimos la coma en punto
		
					}
				};

				
				
				
				
}



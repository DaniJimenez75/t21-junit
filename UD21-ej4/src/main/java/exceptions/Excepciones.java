package exceptions;

import javax.swing.JOptionPane;

import dto.Calcular;



public class Excepciones extends Exception{

	private int codigoExcepcion;

	public Excepciones(int codigoExcepcion) {
		this.codigoExcepcion = codigoExcepcion;
	}
	
	
	@Override
	public String getMessage() {
		
		String mensaje = "";
		
		if(codigoExcepcion == 0) {//Si le enviamos el codigo 0
			mensaje="Operación no valida";
		}
		return mensaje;
	}
	
	
	
}

package Team1.UD21_ej3;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import views.principal;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;


public class AppTest 

{
    @Test
    private static Stream<Arguments> getFormatFixture(){
        return Stream.of(
                Arguments.of(1,2,"+",3),
                Arguments.of(2,2,"-",0),
                Arguments.of(2,2,"*",4),
                Arguments.of(4,0,"/",0));
    }

	@ParameterizedTest
	@MethodSource("getFormatFixture")
	public void testFigura(int n1,int n2, String operacion, int resultado)
	{
	    principal p1= new principal();
	    assertEquals(principal.operaciones(n1,n2,operacion),resultado);
	}

	}

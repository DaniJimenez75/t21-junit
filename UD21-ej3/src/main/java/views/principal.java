package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class principal extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private int operador1, operador2, posoperador=0, ccaracter=0;
	private boolean iteracion=false;
	private String operacion;
	private JTextArea textArea;
	private JLabel rbin, roct, rdec, rhex;

	
	public principal() {
		setTitle("Calculadora");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 576, 403);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton uno = new JButton("1");
		uno.setBounds(29, 269, 59, 23);
		contentPane.add(uno);
		uno.addActionListener(añadir);
		
		JButton dos = new JButton("2");
		dos.setBounds(98, 269, 59, 23);
		contentPane.add(dos);
		dos.addActionListener(añadir);
		
		JButton tres = new JButton("3");
		tres.setBounds(167, 269, 59, 23);
		contentPane.add(tres);
		tres.addActionListener(añadir);
		
		JButton cuatro = new JButton("4");
		cuatro.setBounds(29, 235, 59, 23);
		contentPane.add(cuatro);
		cuatro.addActionListener(añadir);
		
		JButton cinco = new JButton("5");
		cinco.setBounds(98, 235, 59, 23);
		contentPane.add(cinco);
		cinco.addActionListener(añadir);
		
		JButton seis = new JButton("6");
		seis.setBounds(167, 235, 59, 23);
		contentPane.add(seis);
		seis.addActionListener(añadir);
		
		JButton siete = new JButton("7");
		siete.setBounds(29, 201, 59, 23);
		contentPane.add(siete);
		siete.addActionListener(añadir);
		
		JButton ocho = new JButton("8");
		ocho.setBounds(98, 201, 59, 23);
		contentPane.add(ocho);
		ocho.addActionListener(añadir);
		
		JButton nueve = new JButton("9");
		nueve.setBounds(167, 201, 59, 23);
		contentPane.add(nueve);
		nueve.addActionListener(añadir);
		
		JButton cero = new JButton("0");
		cero.setBounds(98, 304, 59, 23);
		contentPane.add(cero);
		cero.addActionListener(añadir);
		
		JButton masmenos = new JButton("+/-");
		masmenos.setBounds(29, 303, 59, 23);
		contentPane.add(masmenos);
		
		JButton igual = new JButton("=");
		igual.setBounds(241, 304, 59, 23);
		contentPane.add(igual);
		igual.addActionListener(resultado);
		
		JButton sumar = new JButton("+");
		sumar.setBounds(241, 269, 59, 23);
		contentPane.add(sumar);
		sumar.addActionListener(conseguiroperacion);
		
		JButton restar = new JButton("-");
		restar.setBounds(241, 235, 59, 23);
		contentPane.add(restar);
		restar.addActionListener(conseguiroperacion);
		
		JButton multiplicar = new JButton("*");
		multiplicar.setBounds(241, 201, 59, 23);
		contentPane.add(multiplicar);
		multiplicar.addActionListener(conseguiroperacion);
		
		JButton dividir = new JButton("/");
		dividir.setBounds(241, 167, 59, 23);
		contentPane.add(dividir);
		dividir.addActionListener(conseguiroperacion);
		
		JButton borrar = new JButton("Borrar");
		borrar.setBounds(147, 167, 79, 23);
		contentPane.add(borrar);
		borrar.addActionListener(eliminar);
		
		JButton c = new JButton("C");
		c.setBounds(29, 167, 59, 23);
		contentPane.add(c);
		c.addActionListener(borrartodo);
		
		JLabel dec = new JLabel("DEC");
		dec.setBounds(29, 86, 46, 14);
		contentPane.add(dec);
		
		JLabel oct = new JLabel("OCT");
		oct.setBounds(29, 111, 46, 14);
		contentPane.add(oct);
		
		JLabel bin = new JLabel("BIN");
		bin.setBounds(29, 136, 46, 14);
		contentPane.add(bin);
		
		JLabel hex = new JLabel("HEX");
		hex.setBounds(29, 61, 46, 14);
		contentPane.add(hex);
		
		rhex = new JLabel("");
		rhex.setBounds(85, 61, 141, 14);
		contentPane.add(rhex);
		
		rdec = new JLabel("");
		rdec.setBounds(85, 86, 141, 14);
		contentPane.add(rdec);
		
		roct = new JLabel("");
		roct.setBounds(85, 111, 141, 14);
		contentPane.add(roct);
		
		rbin = new JLabel("");
		rbin.setBounds(85, 136, 141, 14);
		contentPane.add(rbin);
		
		textArea = new JTextArea();
		textArea.setBounds(329, 56, 200, 271);
		contentPane.add(textArea);
		
		JLabel lblNewLabel = new JLabel("Historial");
		lblNewLabel.setBounds(403, 31, 59, 14);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(29, 28, 271, 20);
		contentPane.add(textField);
		textField.setColumns(10);
	}
	
	ActionListener añadir = new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			JButton v = (JButton) e.getSource();
			textField.setText(textField.getText()+v.getText());
			
			String numeropicado=textField.getText();
			
			ccaracter++;
			
			if (!iteracion) {
				operador1 = Integer.parseInt(numeropicado);
			}else {
				operador2 = Integer.parseInt(numeropicado.substring(posoperador));
			}
		}
	};
	
	ActionListener borrartodo = new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			textField.setText(null);
			
			iteracion=false;
			
			posoperador=0;
			
			ccaracter=0;
		}
		
	};
	
	ActionListener conseguiroperacion = new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {

			
			if (!iteracion) {
				JButton v = (JButton) e.getSource();
				
				ccaracter++;
				
				posoperador = ccaracter;
				
				textField.setText(textField.getText()+v.getText());
				
				operacion = v.getText();
				
				iteracion = true;
				
				
			}
		}
	};
	
	ActionListener eliminar = new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			if(!textField.getText().equals("")) {
				
				ccaracter--;
				
				textField.setText((textField.getText().substring(0,textField.getText().length()-1)));
				
				if (ccaracter<posoperador) {
					iteracion=false;
				}
			}
		}
	};
	
	ActionListener resultado = new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				
				
				if (ccaracter>posoperador&&iteracion) {
					int calculo = operaciones(operador1,operador2, operacion);
					
					String calculostring = String.valueOf(calculo);
					
					String operador1string = String.valueOf(operador1);
					
					String operador2string = String.valueOf(operador2);
					
					textArea.append(operador1string + " " + operacion + " " + operador2string + "=" + calculostring);
					
					textArea.append(System.getProperty("line.separator"));
					
					textField.setText(null);
					
					iteracion=false;
					
					ccaracter=0;
					
					posoperador=0;
					
					rdec.setText(calculostring);
					
					int valor = Integer.parseInt(calculostring);
					
					rhex.setText(Integer.toHexString(valor));
					
					rbin.setText(Integer.toBinaryString(valor));
					
					roct.setText(Integer.toOctalString(valor));
				}
				
					
				

				
			}
		};
		
		public static int operaciones(int n1,int n2, String operacion) {
			int retorn=0;
			
			switch (operacion) {
			case "+":
				retorn = n1 + n2;
				break;
			
			case "-":
				retorn = n1 - n2;
				break;
			
			case "*":
				retorn = n1 * n2;
				break;
			
			case "/":
				try {
					retorn = n1 / n2;
				} catch (java.lang.ArithmeticException e) {
					System.out.println("No se puede dividir entre cero");
				}
				break;
			}
		
			return retorn;
		}
}
